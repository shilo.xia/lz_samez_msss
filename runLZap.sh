source /cvmfs/lz.opensciencegrid.org/LZap/release-5.4.9/x86_64-centos7-gcc8-opt/setup.sh 
#/cvmfs/lz.opensciencegrid.org/LZap/release-5.4.9/x86_64-centos7-gcc8-opt/ProductionSteeringFiles/SR1/
#source setup.sh
for index in {0..0}
do
	export LZAP_INPUT_FILES=filelist/TwoElectronsLowZ_${index}.list
	export LZAP_OUTPUT_FILE=/global/cfs/cdirs/lz/users/qxia/ElectronSimLowZ/ElectronsLowZ_${index}_lzap.root
	while [[ ${#index} -lt 10 ]] ; do
	    index="0${index}"
	done
	echo $LZAP_INPUT_FILES
	echo $LZAP_OUTPUT_FILE
	lzap ${LZAP_INSTALL_DIR}/ProductionSteeringFiles/SR1/RunLZap.py
        #lzap ProductionSteeringFiles/SR1/RunLZap.py
done
