#!/bin/bash

buildType=$1
version=$2


MACFile=TwoElectronSim.mac 
OUTPUT_PATH=/global/cfs/cdirs/lz/users/qxia/SimulationTutorials/Output
SEED=220
NBEAMON=100


if  ([ "$buildType" != "--cvmfs" ] && [ "$buildType" != "--local" ]) || [ -z "$version" ]; then
      echo "Wrong usage: "
      echo "./RunBaccarat.sh --cvmfs build_version"
      echo "./RunBaccarat.sh --local /Path/to/folder/including/your/executables"

      exit 1
fi


if [ "$buildType" == "--cvmfs" ];then

    CVMFS_MAINDIR=/cvmfs/lz.opensciencegrid.org/BACCARAT/release-
    CVMFS_DIR=$CVMFS_MAINDIR$version

    if [ ! -d "$CVMFS_DIR" ]; then
	echo "Impossible to find the build version $version."
	echo "Available build versions on CVMFS: "
	LS=`ls -d $CVMFS_MAINDIR* | sed 's|'${CVMFS_MAINDIR}'||'`
	echo $LS
	exit 1
    fi

    BACC_DIR=$CVMFS_DIR/x86_64-centos7-gcc8-opt

    SETUP=$BACC_DIR/setup.sh
    BACEXE=$BACC_DIR/bin/BACCARATExecutable
    BACConv=$BACC_DIR/bin/BaccRootConverter
    MCTruth=$BACC_DIR/bin/BaccMCTruth

else
    BACC_DIR=$version

    if [ ! -d "$BACC_DIR" ]; then
	echo "The folder ${BACC_DIR} doesn't exist."
	exit 1
    fi

    SETUP=$BACC_DIR/../../../setup.sh
    BACEXE=$BACC_DIR/BACCARATExecutable
    BACConv=$BACC_DIR/BaccRootConverter
    MCTruth=$BACC_DIR/BaccMCTruth

fi


echo "Creating the output directory"

mkdir -p $OUTPUT_PATH

echo "Setting up the environement"
echo "source $SETUP"
source $SETUP

echo "Exporting the variables"
export OUTPUT_PATH=$OUTPUT_PATH
export SEED=$SEED
export NBEAMON=$NBEAMON


echo "Running BACCARAT"
$BACEXE $MACFile

BIN_FILE=$OUTPUT_PATH/TwoElectronSimLowZ_${SEED}.bin

echo "Convert the binary file"
$BACConv $BIN_FILE

ROOT_FILE=$OUTPUT_PATH/TwoElectronSimLowZ_${SEED}.root

echo "Convert to mctruth file"
$MCTruth $ROOT_FILE

echo "Done!"
