#!/usr/bin/env python3

#
# Preping LZ Mock Data for Position Recontruction 2D CNN Regression
# By Andrew Naylor (Oct 2020)
#

#
# TO-DO:
# - just focusing on getting it working with mdc3
# - dask/parallelise code
#      + could seperate truthRQs code 
#        from RQs code
# - profile/optimise parts of code
#   (likely areas of improvement):
#      + get_s2_top_pmt_areas (I think this is the very slow bit)
#      + convert_pmts_to_images_hist2d (actually pretty fast)
#

##IMPORTS##
import sys
import argparse
import uproot
import awkward1 as ak #awk.concatenate is very slow and only 1 cpu
import numpy as np
import numexpr as ne
import h5py


##DEFINITIONS##
# INPUT_RQs =['pulsesTPCHG.chPulseArea_phd','pulsesTPCHG.chID','pulsesTPCHG.pulseID',
#             'pulsesTPCHG.s2Xposition_cm','pulsesTPCHG.s2Yposition_cm',
#             'singleScatters.correctedX_cm','singleScatters.correctedY_cm',
#             'singleScatters.nSingleScatters','singleScatters.s2PulseID']
INPUT_RQs = ['pulsesTPCHG.chPulseArea_phd', 'ss.s2PulseID', 'pulsesTPCHG.chID'] 
INPUT_RQs_tree = 'Events'
INPUT_TruthRQs=['mcTruthEvent.parentPositionX_mm','mcTruthEvent.parentPositionY_mm']
INPUT_TruthRQs_tree = 'RQMCTruth'


##FUNCTIONS##
def gen_PMT_image_from_lzap(lzap_paths):
    """Generate hist2d """
    s2_top_pmt_areas = []

    #Get events which are single scatters
    print(">> Getting single scatter events...")
    single_scatters_mask = gen_mask(lzap_paths, 'Scatters', 'ss.nSingleScatters', 'x==1')


    #### >>> COULD PARALLELISE with dask or something, running 1 file to 1 job >>> ####
    #### >>> Could also split RQs and TruthRQs so they do the jobs seperately  >>> ####

    #Loop Through RQs events
    print(">> Looping through RQ events...")
    mask_start=0
    for chunk in uproot.iterate(lzap_paths, INPUT_RQs_tree, INPUT_RQs, 
                                namedecode='utf-8', awkwardlib=ak):
        mask_stop = mask_start+len(chunk[INPUT_RQs[0]])

        #Take only single scatter events
        data_RQs = {var: chunk[var][single_scatters_mask[mask_start:mask_stop]] for var in INPUT_RQs}

        #Work out s2_top_pmt_areas
        s2_top_pmt_areas.append(get_s2_top_pmt_areas(data_RQs))

        mask_start = mask_stop

    #Looping through TruthRQs events
    mc_pos_x=[]
    mc_pos_y=[]

    print(">> Looping through TruthRQ events...")
    mask_start=0
    for chunk in uproot.iterate(lzap_paths, INPUT_TruthRQs_tree, INPUT_TruthRQs, 
                                namedecode='utf-8', awkwardlib=ak):
        mask_stop = mask_start+len(chunk[INPUT_TruthRQs[0]])

        #Take only single scatter events
        data_TruthRQs = {var: chunk[var][single_scatters_mask[mask_start:mask_stop]] for var in INPUT_TruthRQs}

        mc_pos_x.append(data_TruthRQs['mcTruthEvent.parentPositionX_mm'])
        mc_pos_y.append(data_TruthRQs['mcTruthEvent.parentPositionY_mm'])
        mask_start = mask_stop    
    ####################################################################################

    #Work out which grid the event takes place in
    print(">> Work out grid numbers")
    truth = evt_classifier_grid(np.concatenate(mc_pos_x)/10., np.concatenate(mc_pos_y)/10.)

    #Get PMT positions to convert PMT space to real space
    pmt_x, pmt_y = get_pmt_pos("tpc_pmt_pos.csv")

    #Convert s2_top_pmt_areas into an image
    print(">> Converting s2 top pmt areas into images...")
    images = convert_pmts_to_images_hist2d(np.concatenate(s2_top_pmt_areas), pmt_x, pmt_y)

    return images, truth

def gen_mask(root_files, tree_name, variable, cut_expr):
    """Generate mask from expression, currently setup on only do 1 variable. 
    Write expression with x"""
    mask_list = []
    for arrays in uproot.iterate(root_files, tree_name, variable, namedecode='utf-8'):
        vars()['x'] = arrays[variable]
        mask_list.append(ne.evaluate(cut_expr))
    return np.concatenate(mask_list)

def get_s2_top_pmt_areas(dict_RQs):
    """Get the top PMT area for the s2 pulse"""
    s2_top_pmt_areas = []

    #NEEDS TO BE VECTORIESED 
    #AS INPUT IS CHUNK of events

    for pulse_area, pulse_id, chID in zip(dict_RQs['pulsesTPCHG.chPulseArea_phd'], 
                                          dict_RQs['singleScatters.s2PulseID'], 
                                          dict_RQs['pulsesTPCHG.chID']):
        #Get pulse area + chIDs
        temp_pulse_areas=np.asarray(pulse_area[pulse_id[0]])
        temp_pulse_chIDs=np.asarray(chID[pulse_id[0]])

        #Select top PMTs
        pulse_areas=temp_pulse_areas[temp_pulse_chIDs<253]
        pulse_chIDs=temp_pulse_chIDs[temp_pulse_chIDs<253]

        s2_top_pmt_areas.append(pad_top_pmt_array(pulse_areas, pulse_chIDs))

    return s2_top_pmt_areas

def pad_top_pmt_array(pulse_areas, pulse_chIDs):
    """Take the pmt data(chID + pulseArea) and place it in the right location
    on the grid"""
    #create the empty 253 pmt top array (0-252)
    padded_top_areas=np.zeros(253)
    
    #fill the array
    np.put(padded_top_areas, pulse_chIDs, pulse_areas)
    return padded_top_areas

def get_pmt_pos(file_path):
    """Getting top PMT x,y positions"""
    pmt_pos = np.genfromtxt(file_path, delimiter=',', skip_header=False)
    top_pmts=pmt_pos[:,0]<253
    return pmt_pos[:,5][top_pmts], pmt_pos[:,6][top_pmts]

def convert_pmts_to_images_hist2d(pmt_areas,pmt_pos_x,pmt_pos_y,pixels=64,verbose=10000):
    """Take the pmt grid images and convert it into 2d image with intensity using hist2d"""
    images=[]
    
    xedges = np.linspace(np.min(pmt_pos_x)-13,np.max(pmt_pos_x)+13,num=pixels+1)
    yedges = xedges
    
    for evt_id in range(0, len(pmt_areas)):
        if (evt_id+1)% verbose == 0: print("{0}/{1}".format(evt_id+1,len(pmt_areas)))
        H, xedges, yedges = np.histogram2d(pmt_pos_x, pmt_pos_y, bins=(xedges, yedges), weights=pmt_areas[evt_id])
        H = H.T
        images.append(H)
        
    return np.stack(images)

def evt_classifier_grid(x,y,a=54.,b=81.):
    """Returns grid classification number. a is the length of 
    a single square grid. b is the zero point distance.
    Input X, Y in cm
        6 | 7 | 8
        ---------
        3 | 4 | 5
        ---------
        0 | 1 | 2
    """
    return 3*np.floor((y+b)/a)+np.floor((x+b)/a)

#MISC
def get_files_from_list(path):
    """Load text file contents to array"""
    with open(path, 'r') as file:
        file_paths = file.read().split('\n')
    return [path for path in file_paths if path]

def eprint(*args, **kwargs):
    """Printing to stderr"""
    print(*args, file=sys.stderr, **kwargs)


##MAIN##
def main():
    parser = argparse.ArgumentParser(description="Generate PMT Image from LZ mock data for "
                                                 "position reconstruction 2D CNN Regression"
                                    )
    parser.add_argument('path', type=str, 
                                        help="Path to text file containing list of LZAP files")
    parser.add_argument('-o', '--output', type=str, default='lzap_PMT_images.hdf5', 
                                        help="Output path for h5py file (default: %(default)s)")
    args = parser.parse_args()


    # Get lzap paths from text file
    try:
        lzap_paths = get_files_from_list(args.path)
    except BaseException as err:
        eprint("<<Couldn't find/open file: {}>>".format(err))
        return

    # Generate PMT image from lzap root files
    images, truth = gen_PMT_image_from_lzap(lzap_paths)
    
    # Save output
    with h5py.File(args.output, 'w') as out_file:
        out_file.create_dataset("Images", data=images)
        out_file.create_dataset("Truth", data=truth)

    return

if __name__ == "__main__":
    main()




## Previous code

# def _get_var_from_chunks(chunks, var):
#     return ak.concatenate([chunk[var] for chunk in chunks], axis=0)

# data_RQs = {var: _get_var_from_chunks(chunks_RQs, var) for var in INPUT_RQs}

# def _get_var_from_chunks(chunks, var):
#     return concat_awk([chunk[var] for chunk in chunks])

# def concat_awk(data):
#     return np.concatenate(data, axis=0) #3m16s with awkward1 similar time with awkard #using ak.concatenate +16mins
