#!/bin/bash

buildType=$1
version=$2
SEED=220
OUTPUT_PATH=/global/cfs/cdirs/lz/users/qxia/SimulationTutorials/Output/


if  ([ "$buildType" != "--cvmfs" ] && [ "$buildType" != "--local" ]) || [ -z "$version" ]; then
      echo "Wrong usage: "
      echo "./RunDER.sh --cvmfs build_version"
      echo "./RunDER.sh --local /Path/to/folder/including/your/executables"

      exit 1
fi


if [ "$buildType" == "--cvmfs" ];then

    CVMFS_MAINDIR=/cvmfs/lz.opensciencegrid.org/DER/release-
    CVMFS_DIR=$CVMFS_MAINDIR$version

    if [ ! -d "$CVMFS_DIR" ]; then
	echo "Impossible to find the build version $version."
	echo "Available build versions on CVMFS: "
	LS=`ls -d $CVMFS_MAINDIR* | sed 's|'${CVMFS_MAINDIR}'||'`
	echo $LS
	exit 1
    fi    

    DER_DIR=$CVMFS_DIR/x86_64-centos7-gcc8-opt
    
    SETUP=$DER_DIR/setup.sh
    DEREXE=$DER_DIR/bin/DER

else 
    DER_DIR=$version
    
    if [ ! -d "$DER_DIR" ]; then
	echo "The folder ${DER_DIR} doesn't exist."
	exit 1
    fi
        
    SETUP=$DER_DIR/setup.sh
    LZLAMAEXE=$DER_DIR/DER

fi


echo "Creating the output directory"

mkdir -p $OUTPUT_PATH

echo "Setting up the environement"
source $SETUP

echo "Exporting the variables"
export OUTPUT_PATH=$OUTPUT_PATH


echo "Running the DER"
$DEREXE --outDir $OUTPUT_PATH $OUTPUT_PATH/ElectronSimLowZ_${SEED}_mctruth.root 

echo "Done!"
