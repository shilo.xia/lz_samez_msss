import matplotlib.pyplot as plt
import numpy as np
import h5py
#with np.load('lzap_PMT_images_electrons.npz') as in_file:
with np.load('lzap_PMT_images_Pb212_2.npz') as in_file:
    images = np.array(in_file["Images"])
    truth = np.array(in_file["Truth"])
    truthpos = np.array(in_file["TruthPos"])
print (len(images))
print (len(truthpos))
pmt_pos = np.genfromtxt("tpc_pmt_pos.csv", delimiter=',', skip_header=False)
#select top only
top=pmt_pos[:,0]<253

#select x and y only
pmt_num=pmt_pos[:,0][top]
x=pmt_pos[:,5][top]
y=pmt_pos[:,6][top]
print ("Totol number of events:", len(truth))
print ("Number of MS events:", len(truth[truth==2]))
xedges = np.linspace(np.min(x)-13,np.max(x)+13,num=64)
yedges = xedges
for evt_id in range(len(truth)):
    if truth[evt_id]==2:
        multiplicity = "MS"
    else:
        multiplicity = "SS"
    H = images[evt_id]
 #       H, xedges, yedges = np.histogram2d(x, y, bins=(xedges, yedges), weights=np.ones(253))
#   	H = H.T
        # plt.style.use(mplhep.style.ROOT)
    X, Y = np.meshgrid(xedges, yedges)
    im = plt.pcolormesh(X, Y, H, cmap="Blues")
#        plt.scatter(x,y,color="r",s=10)
    cbar = plt.colorbar(im)
    cbar.ax.set_ylabel('pulsesTPCHG.chPulseArea_phd')
    plt.text(0.5, 0.5, multiplicity, horizontalalignment='center',verticalalignment='center')
    # plt.xlim(np.min(pos_x)-200,np.max(pos_x)+200)
    # plt.ylim(np.min(pos_y)-200,np.max(pos_y)+200)
    plt.show()
