# lz_samez_msss
Scripts for generating mock data files of single and double electrons using full chain sim. CNN for discrminating MS/SS of S2s in the same Z plane using simulated PMT hit patterns. Slides for reference: https://docs.google.com/presentation/d/1i0mOXzlt5oKnAyQ_XfB3O0Ye5qx2mMd8K-AONCSSocM/edit#slide=id.p
Steps for running the scripts (Change input/output file directories wherever necessary):
1. baccarat/RunBaccarat.sh: Generate single(double) electrons using baccarat/ElectronSim.mac or baccarat/TwoElectronSim.mac
2. RunDER.sh: Generate mock waveforms. 
3. runLZap.sh: Process DER output with LZap. Update the LZap version if neeeded.
4. prep_LZMDC3_Data_msss.py: Save rqs including pmt hit patterns in .npz files 
5. msss.ipynb: jupyter notebook that reads in the .npz file and perform CNN training/testing



