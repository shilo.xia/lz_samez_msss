#!/usr/bin/env python3

#
# Preping LZ Mock Data for 2D CNN Regression
#

##IMPORTS##
import sys
import argparse
import uproot
#import awkward1 as ak #awk.concatenate is very slow and only 1 cpu
import numpy as np
import numexpr as ne
import pdb
import glob

##DEFINITIONS##
# INPUT_RQs =['pulsesTPCHG.chPulseArea_phd','pulsesTPCHG.chID','pulsesTPCHG.pulseID',
#             'pulsesTPCHG.s2Xposition_cm','pulsesTPCHG.s2Yposition_cm',
#             'singleScatters.correctedX_cm','singleScatters.correctedY_cm',
#             'singleScatters.nSingleScatters','singleScatters.s2PulseID']
INPUT_RQs = ['pulsesTPCHG.chPulseArea_phd', 'ss.s2PulseID', 'pulsesTPCHG.chID'] 
INPUT_RQs_tree = 'Events'
#INPUT_TruthRQs=['mcTruthEvent.parentPositionX_mm','mcTruthEvent.parentPositionY_mm']
INPUT_TruthRQs = ['mcTruthPulses.pulseIdentifier','mcTruthVertices.positionZ_mm']
INPUT_TruthRQs_tree = 'RQMCTruth'


##FUNCTIONS##
def gen_PMT_image_from_lzap(lzap_paths):
    """Generate hist2d """
    s2_top_pmt_areas = []

    #Get events which are single scatters
    print(">> Getting events...")
#    scatters_mask = gen_mask(lzap_paths, 'RQMCTruth', 'mcTruthPulses.pulseIdentifier','x==1')


    #### >>> COULD PARALLELISE with dask or something, running 1 file to 1 job >>> ####
    #### >>> Could also split RQs and TruthRQs so they do the jobs seperately  >>> ####

    #Loop Through RQs events
    print(">> Looping through RQ events...")
    chPulsearea = []
    s2xychi2 = []
    s2xyarea = []
    pulsearea = []
    chID = []
    nSS = []
    nS2pileUp = []
    nS2other = []
    s2PulseID = []
    s2PulseID_pileUp = []
    s2PulseID_other = []
#    xychi2 = []
    pulseID = []
    vertexnum = []
    pheCount = []
    truthPos_X = []
    truthPos_Y = []
    truthPos_Z = []
    truthEng = []
    for rootfile in sorted(glob.glob("%s/ElectronsLowZ*lzap.root"%lzap_paths)):
#    for rootfile in sorted(glob.glob("%s/*lzap.root"%lzap_paths)):
        infile = uproot.open(rootfile)
        tree = infile["Events"]
        Scattertree = infile["Scatters"]
        MCtree = infile["RQMCTruth"]
        print ("Reading chPulseArea...")
        chPulsearea.append(tree["pulsesTPCHG.chPulseArea_phd"].array())
        print ("Reading chID...")
        chID.append(tree["pulsesTPCHG.chID"].array())
        print ("Reading mercury reconstructed variables...")
        s2xychi2.append(tree["pulsesTPC.s2XYchi2"].array())
        s2xyarea.append(tree["pulsesTPC.s2XYarea_phd"].array())
        pulsearea.append(tree["pulsesTPC.pulseArea_phd"].array())
        print ("Reading nSingleScatters...")
        nSS.append(Scattertree["ss.nSingleScatters"].array())
        nS2pileUp.append(Scattertree["pileUp.nS2"].array())
        nS2other.append(Scattertree["other.nS2s"].array())
        print ("Reading s2PulseID...")
        s2PulseID.append(Scattertree["ss.s2PulseID"].array())
#        xychi2.append(Scattertree["ss.xyChi2"].array())
        s2PulseID_pileUp.append(Scattertree["pileUp.s2PulseIDs"].array())
        s2PulseID_other.append(Scattertree["other.s2PulseIDs"].array())
        print ("Reading mctruth...")
        pulseID.append(MCtree['mcTruthPulses.pulseIdentifier'].array())
        vertexnum.append(MCtree['mcTruthPulses.vertexNumber'].array())
        pheCount.append(MCtree['mcTruthPulses.pheCount'].array())
        truthPos_X.append(MCtree['mcTruthVertices.positionX_mm'].array())
        truthPos_Y.append(MCtree['mcTruthVertices.positionY_mm'].array())
        truthPos_Z.append(MCtree['mcTruthVertices.positionZ_mm'].array())
        truthEng.append(MCtree['mcTruthVertices.energyDep_keV'].array())
    chPulsearea = np.concatenate(chPulsearea)
    s2xychi2 = np.concatenate(s2xychi2)
    s2xyarea = np.concatenate(s2xyarea)
    pulsearea = np.concatenate(pulsearea)
    chID = np.concatenate(chID)
    nSS = np.concatenate(nSS)
    nS2other = np.concatenate(nS2other)
    nS2pileUp = np.concatenate(nS2pileUp)
    s2PulseID = np.concatenate(s2PulseID)
    s2PulseID_pileUp = np.concatenate(s2PulseID_pileUp)
    s2PulseID_other = np.concatenate(s2PulseID_other)
    pulseID = np.concatenate(pulseID)
    vertexnum = np.concatenate(vertexnum)
    pheCount = np.concatenate(pheCount)
    truthPos_X = np.concatenate(truthPos_X)
    truthPos_Y = np.concatenate(truthPos_Y)
    truthPos_Z = np.concatenate(truthPos_Z)
    truthEng = np.concatenate(truthEng)
    NEvent = len(chPulsearea)
    truth = []
    truthpos = []
    truthenergy = []
    XYchi2 = []
    XYarea = []
    S2area = []
    for i in range(NEvent):
        if (i%1000==0): print ('Processing event %d...'%i)
        flag = False
        nvertex = 0
        gpts = (pulseID[i]==2)
        #Take only single scatter events 
        if (nSS[i]>0 or nS2pileUp[i]==1 or nS2other[i]==1):
            ntruthS2 = np.count_nonzero(gpts)
            if (ntruthS2 == 1): 
                nvertex = 1
                flag = True
                v = vertexnum[i,gpts]
                j = v[0]
                truthpos.append([[truthPos_X[i,j],truthPos_Y[i,j],truthPos_Z[i,j]],[-999,-999,-999]])
                truthenergy.append([truthEng[i,j],0])
            elif (ntruthS2 == 2): 
                nvertex = 0
                v = vertexnum[i,gpts]
                for j in v:
                    if (truthPos_X[i,j]*truthPos_X[i,j]+truthPos_Y[i,j]*truthPos_Y[i,j]<688**2 and truthPos_Z[i,j]>20 and truthPos_Z[i,j]<1326):
                        if (nvertex == 0):
                            x0 = truthPos_X[i,j]
                            y0 = truthPos_Y[i,j]
                            z0 = truthPos_Z[i,j]
                            eng0 = truthEng[i,j]
                            nvertex+=1
                        elif (nvertex == 1 and abs(truthPos_Z[i,j]-z0)<5):
                            x1 = truthPos_X[i,j]
                            y1 = truthPos_Y[i,j]
                            z1 = truthPos_Z[i,j]
                            eng1 = truthEng[i,j]
                            flag = True
                            nvertex+=1
                if (nvertex>2): 
                    flag = False
                if flag:                
                    truthpos.append([[x0,y0,z0],[x1,y1,z1]])
                    truthenergy.append([eng0,eng1])
            if (flag):
                if (nvertex==2):
                    print ("Event",i,": first scatter pos",[x0,y0,z0],"second svcatter pos",[x1,y1,z1],"pheCount:",pheCount[i,gpts])
                if (nSS[i]>0):
                    data_RQs = {"pulsesTPCHG.chPulseArea_phd":chPulsearea[i],'ss.s2PulseID':s2PulseID[i],'pulsesTPCHG.chID':chID[i]}
                    XYchi2.append(s2xychi2[i][s2PulseID[i]])
                    XYarea.append(s2xyarea[i][s2PulseID[i]])
                    S2area.append(pulsearea[i][s2PulseID[i]])
#                    XYchi2.append(xychi2[i])
                elif (nS2pileUp[i]==1):
                    data_RQs = {"pulsesTPCHG.chPulseArea_phd":chPulsearea[i],'ss.s2PulseID':s2PulseID_pileUp[i],'pulsesTPCHG.chID':chID[i]}
                    XYchi2.append(s2xychi2[i][s2PulseID_pileUp[i][0]])
                    XYarea.append(s2xyarea[i][s2PulseID_pileUp[i][0]])
                    S2area.append(pulsearea[i][s2PulseID_pileUp[i][0]])
                elif (nS2other[i]==1):
                    data_RQs = {"pulsesTPCHG.chPulseArea_phd":chPulsearea[i],'ss.s2PulseID':s2PulseID_other[i],'pulsesTPCHG.chID':chID[i]}
                    XYchi2.append(s2xychi2[i][s2PulseID_other[i][0]])
                    XYarea.append(s2xyarea[i][s2PulseID_other[i][0]])
                    S2area.append(pulsearea[i][s2PulseID_other[i][0]])
                #Work out s2_top_pmt_areas
                s2_top_pmt_areas.append(get_s2_top_pmt_areas(data_RQs))
                truth.append(ntruthS2)
    print ("Total number of events:",NEvent)
    print ("Number of events passing cuts:",len(truth))
    #Get PMT positions to convert PMT space to real space
    pmt_x, pmt_y = get_pmt_pos("tpc_pmt_pos.csv")
    #Convert s2_top_pmt_areas into an image
    print(">> Converting s2 top pmt areas into images...")
#    truth,truthpos,s2_top_pmt_areas = zip(*sorted(zip(truth,truthpos,s2_top_pmt_areas)))
#    truthpos = [x for _,x in sorted(zip(truth,truthpos))]
#    s2_top_pmt_areas = [x for _,x in sorted(zip(truth,s2_top_pmt_areas))]
#    truth = np.array(sorted(truth))
#    nkeep = len(truth[truth==2])
#    s2_top_pmt_areas = s2_top_pmt_areas[-2*nkeep:]
#    truth = truth[-2*nkeep:]
#    truthpos = truthpos[-2*nkeep:]
    truth=np.array(truth)
    truthpos=np.array(truthpos)
    truthenergy=np.array(truthenergy)
    XYchi2 = np.array(XYchi2)
    XYarea = np.array(XYarea)
    S2area = np.array(S2area)
    print ("Number of SS events passing cuts:",len(truth[truth==1]))
    print ("Number of MS events passing cuts:",len(truth[truth==2]))
    images = convert_pmts_to_images_hist2d(np.array(s2_top_pmt_areas), pmt_x, pmt_y)
    return images, truth, truthpos, truthenergy, XYchi2, XYarea, S2area

def gen_mask(root_files, tree_name, variable, cut_expr):
    """Generate mask from expression, currently setup on only do 1 variable. 
    Write expression with x"""
    mask_list = []
#    for arrays in uproot.iterate("%s/*.root:%s"%(root_files,tree_name), variable, namedecode='utf-8'):
    for arrays in uproot.iterate("%s/*lzap.root:%s"%(root_files,tree_name), variable):
        elelist = []
        for element in arrays[variable]:
            elelist.append(np.count_nonzero(element == 2))
        vars()['x'] = np.array(elelist)
        mask_list.append(ne.evaluate(cut_expr))
    return np.concatenate(mask_list)

def get_s2_top_pmt_areas(dict_RQs):
    """Get the top PMT area for the s2 pulse"""
    s2_top_pmt_areas = []

    #NEEDS TO BE VECTORIESED 
    #AS INPUT IS CHUNK of events

    pulse_area, pulse_id, chID = dict_RQs['pulsesTPCHG.chPulseArea_phd'], dict_RQs['ss.s2PulseID'], dict_RQs['pulsesTPCHG.chID']
    #Get pulse area + chIDs
    temp_pulse_areas=np.asarray(pulse_area[pulse_id])
    temp_pulse_chIDs=np.asarray(chID[pulse_id])

    #Select top PMTs
    pulse_areas=temp_pulse_areas[temp_pulse_chIDs<253]
    pulse_chIDs=temp_pulse_chIDs[temp_pulse_chIDs<253]
    
    s2_top_pmt_areas=pad_top_pmt_array(pulse_areas, pulse_chIDs)

    return s2_top_pmt_areas#.tolist()

def pad_top_pmt_array(pulse_areas, pulse_chIDs):
    """Take the pmt data(chID + pulseArea) and place it in the right location
    on the grid"""
    #create the empty 253 pmt top array (0-252)
    padded_top_areas=np.zeros(253)
    
    #fill the array
    np.put(padded_top_areas, pulse_chIDs, pulse_areas)
    return padded_top_areas

def get_pmt_pos(file_path):
    """Getting top PMT x,y positions"""
    pmt_pos = np.genfromtxt(file_path, delimiter=',', skip_header=False)
    top_pmts=pmt_pos[:,0]<253
    return pmt_pos[:,5][top_pmts], pmt_pos[:,6][top_pmts]

def convert_pmts_to_images_hist2d(pmt_areas,pmt_pos_x,pmt_pos_y,pixels=64,verbose=10000):
    """Take the pmt grid images and convert it into 2d image with intensity using hist2d"""
    images=[]
    
    xedges = np.linspace(np.min(pmt_pos_x)-13,np.max(pmt_pos_x)+13,num=pixels+1)
    yedges = xedges
    
    for evt_id in range(0, len(pmt_areas)):
        if (evt_id+1)% verbose == 0: print("{0}/{1}".format(evt_id+1,len(pmt_areas)))
        H, xedges, yedges = np.histogram2d(pmt_pos_x, pmt_pos_y, bins=(xedges, yedges), weights=pmt_areas[evt_id])
        H = H.T
        images.append(H)
        
    return np.stack(images)

def evt_classifier_grid(x,y,a=54.,b=81.):
    """Returns grid classification number. a is the length of 
    a single square grid. b is the zero point distance.
    Input X, Y in cm
        6 | 7 | 8
        ---------
        3 | 4 | 5
        ---------
        0 | 1 | 2
    """
    return 3*np.floor((y+b)/a)+np.floor((x+b)/a)

#MISC
def get_files_from_list(path):
    """Load text file contents to array"""
    with open(path, 'r') as file:
        file_paths = file.read().split('\n')
    return [path for path in file_paths if path]

#def eprint(*args, **kwargs):
 #   """Printing to stderr"""
  #  print(*args, file=sys.stderr, **kwargs)


##MAIN##
def main():
    parser = argparse.ArgumentParser(description="Generate PMT Image from LZ mock data for "
                                                 "position reconstruction 2D CNN Regression"
                                    )
    parser.add_argument('path', type=str, 
                                        help="Path to text file containing list of LZAP files")
    parser.add_argument('-o', '--output', type=str, default='lzap_PMT_images.npz', 
                                        help="Output path for h5py file (default: %(default)s)")
    args = parser.parse_args()

    lzap_paths = args.path
    # Get lzap paths from text file
#    try:
#    lzap_paths = get_files_from_list(args.path)
 #   except BaseException as err:
  #      eprint("<<Couldn't find/open file: {}>>".format(err))
   #     return

    # Generate PMT image from lzap root files
    images, truth, truthpos,truthenergy,XYchi2,XYarea,S2area = gen_PMT_image_from_lzap(lzap_paths)
    # Save output
    np.savez(args.output,Images=images,Truth=truth,TruthPos=truthpos,TruthEng=truthenergy,XYchi2=XYchi2,XYarea=XYarea,S2area=S2area)

    return

if __name__ == "__main__":
    main()




## Previous code

# def _get_var_from_chunks(chunks, var):
#     return ak.concatenate([chunk[var] for chunk in chunks], axis=0)

# data_RQs = {var: _get_var_from_chunks(chunks_RQs, var) for var in INPUT_RQs}

# def _get_var_from_chunks(chunks, var):
#     return concat_awk([chunk[var] for chunk in chunks])

# def concat_awk(data):
#     return np.concatenate(data, axis=0) #3m16s with awkward1 similar time with awkard #using ak.concatenate +16mins
